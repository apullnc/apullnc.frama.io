document.addEventListener('DOMContentLoaded', () => {
    // Gestion des onglets
    const onHashChange = () => {
        const redirectDefault = () => window.location.hash = document.querySelector('.page').id;
        if (window.location.hash) {
            const currentPage = window.location.hash.substring(1);
            const links = document.querySelectorAll('#menu a');
            links.forEach(previous => previous.classList.remove('selected'));
            Array.from(links).filter(link => link.getAttribute('href') === '#' + currentPage).forEach(link => link.classList.add('selected'));

            const pages = document.querySelectorAll('.page')
            pages.forEach(page => page.classList.remove('selected'));
            const selectedPages = Array.from(pages).filter(page => page.id === currentPage);
            selectedPages.forEach(page => page.classList.add('selected'));
            if (selectedPages.length === 0) {
                redirectDefault();
            }

            if (currentPage === 'association') {
                const map = document.getElementById('map-acaf');
                if (!map.getAttribute('src')) {
                    // Chargement de l'iframe
                    map.setAttribute('src', 'https://www.openstreetmap.org/export/embed.html?bbox=166.44264042377475,-22.266557381129697,166.4461809396744,-22.264360617341683&layer=mapnik&marker=-22.265459003546216,166.44441068172455');
                }
            }
        }
    };
    window.addEventListener('hashchange', onHashChange);
    onHashChange();

    // Gestion du menu
    document.querySelector('#menu-toggle').addEventListener('click', () => {
        const menu = document.querySelector('#menu');
        if (menu.classList.contains('open')) {
            menu.classList.remove('open');
        } else {
            menu.classList.add('open');
        }
    });
});

