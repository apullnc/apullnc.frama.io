# Apull NC

APULL est une association ayant pour objectif de promouvoir le logiciel libre en Nouvelle-Calédonie.

## Contenu du dépôt

Ce dépôt GIT contient les sources du site apull.asso.nc.

## Fonctionnement des pages / onglets

La page active est déterminée par le hash dans l'URL. Le changement de page se fait via le javascript.

La page active et l'entrée dans le menu possèdent la classe CSS selected.

Les pages non marquées inactive (non marquée avec la classe 'selected') ne sont pas affichées.

Pour ajouter une page, il faut créer une div avec la classe "page" sous l'élément main.

## Responsive-Design

Le site est d'abord développé pour les petits écrans. Le fichier 'responsive.css' permet de surcharger les styles sur les écrans plus grand.